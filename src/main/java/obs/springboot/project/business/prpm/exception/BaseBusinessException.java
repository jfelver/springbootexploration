package obs.springboot.project.business.prpm.exception;


public class BaseBusinessException extends BaseException
{	
	/**
	 * Default constructor
	 */
	public BaseBusinessException ()
	{
		super();
	}
	
	/**
	 * Create an exception with an informational message
	 * 
	 * @param message      The message to attach to the exception
	 */	
	public BaseBusinessException (String message)
	{
		super(message);
	}

	/**
	 * Message and wrapped exception constructor
	 * 
	 * @param message      The message to attach to the exception
	 * @param cause        The exception being wrapped by this exception
	 */
	public BaseBusinessException(String message, Throwable cause)
	{
		super(message, cause);
	}
}