package obs.springboot.project.business.prpm.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import obs.springboot.project.business.prpm.domain.CodeInfo;
import obs.springboot.project.business.prpm.exception.BaseBusinessException;
import obs.springboot.project.business.prpm.service.FSATableProcessor;

@RestController
@RequestMapping("/FSA")
public class FSAController
{
	@Autowired
	private FSATableProcessor fsaService;
		
	   @GetMapping
	   public ResponseEntity<ArrayList<CodeInfo>> getAllCodes() throws BaseBusinessException
	   {
		System.out.println("Its the Rest API for get all Codes");
		return new ResponseEntity<ArrayList<CodeInfo>>(fsaService.getAllCodes(), HttpStatus.OK);
	   }
			
	   @GetMapping("/{codeId}")
	   public ResponseEntity<CodeInfo> getCodeInfo(@PathVariable("codeId") int codeId) throws BaseBusinessException
	   {
		System.out.println("Its the Rest API for get codes for specific Id.");
        return new ResponseEntity<CodeInfo>(fsaService.getCodeInfo(codeId), HttpStatus.OK);
	   }
		
}
