package obs.springboot.project.business.prpm.service;

import java.util.ArrayList;
import obs.springboot.project.business.prpm.dao.FSADao;
import obs.springboot.project.business.prpm.domain.CodeInfo;
import obs.springboot.project.business.prpm.exception.BaseBusinessException;

public class FSATableProcessor {
	
	/**
	 * Constructor
	 */
	public FSATableProcessor() 
	{}

	
	/*
	 * This method returns the info object for an given id - mock up 
	 * 
	 * @return Info Object with details
	 * @throws BaseErrorBusinessException upon an application error
	*/
	public CodeInfo getCodeInfo(int codeTableID) throws BaseBusinessException
	{
    	System.out.println("Get Info for a given Id.");
    	String tableName = "";
   		try
    		{
    			tableName = fsaTables[codeTableID];
    		}
    		catch(ArrayIndexOutOfBoundsException aiob)
    		{
    			throw new BaseBusinessException(
    				"Invalid Input code table id|" + codeTableID + "| ",aiob);
    		}
    		CodeInfo ci = new CodeInfo();
    		ci.setCodeTableId(codeTableID);
    		ci.setCode(tableName);
    		ci.setDescription("This is a test description only");
    		System.out.println("Done got info for the given Id.");
    		return ci;
    	}

	
	/*
	 * This method returns list of all the codes present in the database - mock up 
	 * 
	 * @return List Object containing all CodeInfo objects
	 * @throws BaseErrorBusinessException upon an application error
	*/
	public ArrayList<CodeInfo> getAllCodes() throws BaseBusinessException
	{
    	System.out.println("getCodes() retrieving ALL codes.");
		ArrayList<CodeInfo> codeListInfo= new ArrayList<CodeInfo>();
		
	 	CodeInfo ci = new CodeInfo();
		ci.setCodeTableId(1);
		ci.setCode("CQR_SPCL_INSTR");
		ci.setDescription("CQR_SPECIAL_INSTRUCTIONS");

   		CodeInfo ci2 = new CodeInfo();
		ci2.setCodeTableId(2);
		ci2.setCode("DLVR_METH");
		ci2.setDescription("DELIVERY_METHOD");

   		CodeInfo ci3 = new CodeInfo();
		ci3.setCodeTableId(3);
		ci3.setCode("ENG_DATA_PRI");
		ci3.setDescription("RED_PRIORITY");

		codeListInfo.add(ci);
		codeListInfo.add(ci2);
		codeListInfo.add(ci3);
		
    	System.out.println("Done get all codes.");
		return  codeListInfo;
	}
	
	
	/*
	 * Helper method to get table name back corresponding to the index passed in
	 * May be replaced by database call
	 */
	private static final String[] fsaTables =
		{ 
			"", 										//0 -
			FSADao.CONTRACT_QUALITY_REQUIREMENTS,	//1  "CQR"
			FSADao.CQR_SPECIAL_INSTRUCTIONS,  	//2  "CQR_SPCL_INSTR", 
			FSADao.DELIVERY_METHOD,				//3  "DLVR_METH", 
			FSADao.RED_PRIORITY,					//4  "ENG_DATA_PRI"
			FSADao.RED_STATUS,					//5  "RED_STAT" 
			FSADao.ENGINEERING_DATA_TYPE,			//6  "TYPE_OF_DATA", 
			FSADao.DISTRIBUTION_CODES,			//7  "DISTR_CDS", 
			FSADao.FURNISHED_METHODS,				//8  "FURN_MTHD", 
			FSADao.PROPRIETARY_RIGHTS,			//9  "PROP_RIGHTS", 
			FSADao.SCREEN_CANCELLATION,			//10 "SCRN_CNCL", 
			FSADao.SCREENING_SOURCE_TYPE,			//11 "SCRN_SRC_TY", 
			FSADao.ITEM_MARKING,					//12 "ITM_MRK",
			FSADao.UNIT_OF_ISSUE,					//13 "UNIT_OF_ISS", 
			FSADao.PSEUDO_CODE,			        //14 "PSEUDO_CODES", 
			FSADao.LINE_ITEM_TYPE,				//15 "TYP_LIN_ITM",
			FSADao.SCREENING_PRIORITY,			//16 "SCRN_PRI", 
			FSADao.SCREENING_PRIORITY,			//17 "SCRN_PRI", 
			FSADao.PURCHASE_LINE_ITEM_PRIORITY,	//18 "PR_MIPR_PRIORITY", 
			FSADao.REQUIREMENT_TYPE,				//19 "RQMT_TY", 
			FSADao.DOCUMENTATION_TYPE,			//20 "DOC_TY",
			FSADao.CAGE,							//21 "CAGE", 
			FSADao.FSC,							//22 "FED_SUP_CLAS",
			FSADao.MMAC,							//23 "MTL_MGT_AGG",
			FSADao.UID_FAR_CLAUSE,				//24 "UID_FAR_CLAUSE", 
			FSADao.PROCUREMENT_ACTIVITY,			//25 "PROCUR_ACTY"
			FSADao.CANCEL_AMEND_REASON,			//26 "CNCL_AMEND_RSN
			"",									//27 "FUNDS_CITE" not in CodeTableDao
			FSADao.INSPECT_ACCEPT,				//28 "INSP_ACPT"
			FSADao.REQUISITION_PRIORITY,			//29 "PUR_RQST_TY"
			FSADao.SCREEN_THRESHOLD,			    //30 "THRS_DOL"
			FSADao.BOILERPLATE,				    //31 "POL_STMT"
			FSADao.CONTRACT_ITEM_CATEGORY,		//32 "CNTRCT_ITM_CTGR"
			FSADao.COMMODITY_COUNCIL,   	    	//33 "CMDTY_CONCL"
			FSADao.DELIVERY_EVENT,				//34 "DELVR_EVENT
			FSADao.COMMON_WORKBASKET_DESC,		//35 "COM_WKBSKT_DESC"
			FSADao.IMPEDIMENT_CODES,				//36 "IMPDMNT"
			FSADao.IMPEDIMENT_ACTION_TAKEN,		//37 "IMPDMNT_ACT_TK"
			FSADao.RNCC,							//38 "RNCC"
			FSADao.RNVC,							//39 "RNVC"
			FSADao.UNIT_OF_MEASURE_LINEAR,		//40 "UNIT_OF_MEASURE_LINEAR"
			FSADao.UNIT_OF_MEASURE_WEIGHT, 		//41 "UNIT_OF_MEASURE_WEIGHT"
			FSADao.UNIT_OF_MEASURE_ALL,			//42 "UNIT_OF_MEASURE_ALL"
			FSADao.CAGE_PART_XREF,				//43 "CAGE_PART_XREF"
			FSADao.MARKING_TYPE, 					//44 "MARKING_TYPE"
			FSADao.MARKING_METHOD, 			    //45 "MARKING_METHOD"
			//	     The IUID_DOLLAR_THRESHOLD commented out as this is maintained in SYS_PARMS Table		
			"",	//		CodeTableDao.IUID_DOLLAR_THRESHOLD,			//46 "IUID_DOLLAR_THRESHOLD"
			FSADao.QUANTITY_UNIT_PACK, 			//47 "QUANTITY_UNIT_PACK"
			FSADao.SDN_SUB_TYPE, 					//48 "SDN_SUB_TYPE"
			FSADao.BPN_CODES, 					//49 "BPN"
			FSADao.TEXT_ACTIVITY_TYPE, 			//50 "ARTFCT_TY" - user defined text activity types
			FSADao.VENDOR_TRNSP_INSTRUCTION, 		//51 "VENDOR_TRNSP_INS" - user defined vendor transportation instructions
			FSADao.VENDOR_PKG_INSTRUCTION, 		//52 "VENDOR_PKG_INS" - user defined vendor packaging instructions
	        FSADao.PI_FUNDS_CERTIFICATION_APPLICABILITY_PI_STATEMENT,   //53 "FUNDS_CERT_APPL_STMT" - Funds Certification Applicability Statements
	        FSADao.PI_FUNDS_CERTIFICATION_APPLICABILITY,   //54 "FUNDS_CERT_APPL_STMT" - Funds Certification Applicability Statements
	        FSADao.PI_FUNDS_CERTIFICATION_APPLICABILITY_VIEW,   //55 "FUNDS_CERT_APPL_STMT_V" - Funds Cert Applicability records joined with Statements
	        FSADao.PI_FUNDS_CERTIFICATION_APPLICABLE_PI_VIEW,   //56 "FUNDS_CERT_APPL_V" - PIs with their Fund Cert Applicability flag applied
		};
	
}