package obs.springboot.project.business.prpm.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

@SuppressWarnings("serial")
public class BaseObject implements Serializable
{
	/* values passed to methods invoked through reflection */
	/** empty object array */
	public static final Object[] NO_ARGS   = new Object[] {};
	/** empty class array */
	public static final Class<?>[]  NO_PARAMS = new Class[] {};
	
	/* the indentation level of the current line */
	private String tabs = "";
	
	/* the current output of the toString() method */
	private StringBuffer values = null;
	
	/* the text displayed around object names */
	private static final String SEPARATOR 
		= "********************************************************************************";

	
	/* maximum number of entries in a list or array to include in the toString() output */
	/* (used to limit the amount of logger output for large objects) */
	private static final int MAX_DISPLAY_ELEMENTS = 100;

	
	/**
	 * Creates a deep copy of the current object and returns it.
	 * 
	 * @return    A copy of this object
	 */
	public BaseObject duplicate()
	{	
		try(final ByteArrayOutputStream outBytes = new ByteArrayOutputStream();
			final ObjectOutputStream outObject = new ObjectOutputStream(outBytes);
			)
		{
			outObject.writeObject(this);
			final byte[] bytes = outBytes.toByteArray();
			
			try(final ByteArrayInputStream inBytes = new ByteArrayInputStream(bytes);
				final ObjectInputStream inObject = new ObjectInputStream(inBytes);
				){
					return (BaseObject)inObject.readObject();
				}
		}
		catch (IOException ex)
		{
			throw new RuntimeException("Unable to clone object " + 
					this.getClass(), ex);
		}
		catch (ClassNotFoundException ex)
		{
			throw new RuntimeException("Unable to clone object " + 
					this.getClass(), ex);
		}
	}
	
	/**
	 * Creates a pretty-printed string containing all values in this
	 * value object. Sub-objects, lists, and arrays will be printed in a 
	 * recursive fashion.
	 * 
	 * @return    A formatted String with all object values
	 */
	public String toString()
	{
        values = new StringBuffer("\n");
		printObject(this);
		String result = values.toString();
		values.setLength(0);
		values = null;
		return result;
	}
	
	/**
	 * Writes the values of the given object to the values StringBuffer.
	 * 
	 * @param obj    The object to inspect and print
	 */
	@SuppressWarnings("unchecked")
	private void convert(Object object)
	{
		try
		{
			Class<? extends Object> objectClass = object.getClass();
			
			// determine the type of the current object and call
			// the appropriate method to print it out
			if (objectClass.isArray())
			{
				printArray((Object[])object);
			}
			else if (objectClass.toString().equals(String.class.toString()) || 
					 objectClass.toString().equals(Integer.class.toString()) ||
					 objectClass.toString().equals(Boolean.class.toString()) ||
					 objectClass.toString().equals(Long.class.toString()) ||
					 objectClass.toString().equals(Character.class.toString()))
			{
				values.append(tabs + object.toString() + "\n");
			}
			else if (object instanceof List)
			{
				printList((List<Object>)object);
			}
			else if (object instanceof Hashtable)
			{
				printHashtable((Hashtable<Object, Object>)object);
			}
			else if (object instanceof HashMap)
			{
				printHashMap((HashMap<Object, Object>)object);
			}
			else if (object instanceof Class)
			{
				values.append(tabs + URLEncoder.encode(object.toString(), StandardCharsets.UTF_8.toString()) + "\n");
			}
			else
			{
				printObject(object);
			}
		}
		catch (Exception e)
		{
			//FORTIFY: For usage where multiple exception types are all handled 
			//the same way, the code is more readable and maintainable by 
			//handling all exception types with a single broad catch statement
			values.append(tabs + "--> Unable to print object " 
							   + object.getClass() 
							   + " due to exception: " + e.getMessage() + "\n");
			decreaseTabLevel();
			values.append(tabs + SEPARATOR + "\n");
		}
	}

	/**
	 * Prints an Object to the values buffer.
	 * 
	 * @param object    The Object to print
	 */
	private void printObject(Object object)
	{
		Class<? extends Object> objectClass = object.getClass();		
		String className = objectClass.getName();
				
		printTitle(className);
		increaseTabLevel();

		// print a value object by calling all its "get" methods
		int maxLength = 0;
		Vector<DataItem> items = new Vector<>();
		Method objectMethods[] = objectClass.getMethods();

		for (int x = 0; x < objectMethods.length; x++)
		{
			if (objectMethods[x].getParameterTypes().length > 0)
			{
				continue;
			}
			
			String methodName = objectMethods[x].getName();
			
			// ignore some methods
			if ((!methodName.startsWith("get") && 
					!methodName.startsWith("is")) ||
				methodName.equals("getFactory") || 
				methodName.equals("getClass") ||
				methodName.equals("getEJBObject"))
			{
				continue;
			}

			try
			{
				// invoke the "get" method to determine out the field value
				Object methodReturn 
						= objectMethods[x].invoke(object, NO_ARGS);
						
				if (methodName.startsWith("get"))
				{	
				methodName = methodName.substring(3);
				}
				else
				{
					methodName = methodName.substring(2);
				}	
				
				if (methodName.length() > maxLength)
				{
					maxLength = methodName.length();
				}

				// take action based on the type of return
				if (methodReturn == null)
				{
					items.add(new DataItem(methodName, "~~~[null]~~~"));
				}
				else if (methodReturn.getClass().toString().equals(String.class.toString()) ||
					methodReturn.getClass().toString().equals(Integer.class.toString()) ||
					methodReturn.getClass().toString().equals(Boolean.class.toString()) ||
					methodReturn.getClass().toString().equals(Long.class.toString()) ||
					methodReturn.getClass().toString().equals(Character.class.toString()))
				{
					items.add(new DataItem(methodName, methodReturn));
				}
				else
				{
					// print out the object returned by the "get" method
					if (methodReturn instanceof List ||
						methodReturn.getClass().isArray())
					{
						int size;
						if (methodReturn instanceof List)
						{
							size = ((List<?>)methodReturn).size();
						}
						else
						{
							size = ((Object[])methodReturn).length;
						}
						values.append(tabs + methodName + "[" + size + "]\n");
					}
					else
					{
						values.append(tabs + methodName + "\n");
					}
					
					if (methodName.equalsIgnoreCase("draftSave") && 
					    methodReturn != null)
					{
						values.append(tabs + "    " + objectMethods[x].getName() + "() not null\n");
					}
					else
					{
						convert(methodReturn);
					}
				}
			}
			catch (Exception e)
			{
				//FORTIFY: For usage where multiple exception types are all handled 
				//the same way, the code is more readable and maintainable by 
				//handling all exception types with a single broad catch statement
				values.append(tabs + "--> Error calling " 
							  + objectMethods[x].getName() + ": " + e.toString() 
							  + "\n");
			}
		}

		// sort the (field, value) list alphabetically and print it out
		DataItem[] itemArray 
				= (DataItem[])items.toArray(new DataItem[items.size()]);
				
		Arrays.sort(itemArray, new DataItemComparator());
		for (int x = 0; x < itemArray.length; x++)
		{
			printData(itemArray[x].name, itemArray[x].value, maxLength);
		}
		decreaseTabLevel();
		values.append(tabs + SEPARATOR + "\n");
	}

	/**
	 * Prints an array of Objects to the values buffer.
	 *  
	 * @param array    The array to print
	 */
	private void printArray(Object[] array)
	{
		increaseTabLevel();
		for (int x = 0; x < array.length; x++)
		{
			if (x > MAX_DISPLAY_ELEMENTS)
			{
				values.append(tabs + "\n*** objects in array beyond " + MAX_DISPLAY_ELEMENTS + " are not displayed ***\n");
				break;
			}

			convert(array[x]);
		}
		decreaseTabLevel();
	}

	/**
	 * Prints the contents of a List to the values buffer.
	 *  
	 * @param list    The List to print
	 */
	private void printList(List<Object> list)
	{
		increaseTabLevel();

		Object[] items = list.toArray(new Object[list.size()]);

		for (int x = 0; x < items.length; x++)
		{
			if (x > MAX_DISPLAY_ELEMENTS)
			{
				values.append(tabs + "\n*** objects in list beyond " + MAX_DISPLAY_ELEMENTS + " are not displayed ***\n");
				break;
			}
 			if (items[x].getClass().toString().equals(String.class.toString()) || 
				items[x].getClass().toString().equals(Integer.class.toString()))
			{
				values.append(tabs + "[" + x + "] " 
								   + items[x].toString() + "\n");
			}
			else
			{
				values.append(tabs + "[" + x + "]\n");
				convert(items[x]);
			}
		}
		decreaseTabLevel();
	}

	/**
	 * Prints the contents of a Hashtable to the values buffer.
	 *  
	 * @param table    The Hashtable to print
	 */
	private void printHashtable(Hashtable<Object, Object> table)
	{
		increaseTabLevel();

		values.append(tabs + "Hashtable\n");
		
		increaseTabLevel();
		
		Enumeration<Object> keys = table.keys();
		while (keys.hasMoreElements())
		{
			Object key = keys.nextElement();

			if (key instanceof String ||
				key instanceof Integer ||
				key instanceof Boolean ||
				key instanceof Long ||
				key instanceof Character)
			{
				values.append(tabs + "|" + key.toString() + "| --> ");
			}
			else
			{
				values.append(tabs + "key   |\n");
			
				convert(key);
				values.append("|\n");
			}
			
			Object value = table.get(key);
			
			if (value instanceof String ||
				value instanceof Integer ||
				value instanceof Boolean ||
				value instanceof Long ||
				value instanceof Character)
			{
				values.append("|" + value.toString() + "|\n");
			}
			else
			{
				values.append(tabs + "value |\n");
				convert(value);
				values.append("|\n");
				values.append(tabs + SEPARATOR + "\n");				
			}
		}
		
		decreaseTabLevel();
		decreaseTabLevel();
	}
	
	/**
	 * Prints the contents of a HashMap to the values buffer.
	 *  
	 * @param map    The HashMap to print
	 */
	private void printHashMap(HashMap<Object, Object> map)
	{
		increaseTabLevel();

		values.append(tabs + "Hashtable\n");
		
		increaseTabLevel();
		
		Iterator<Object> keys = map.keySet().iterator();
		while (keys.hasNext())
		{
			Object key = keys.next();

			if (key instanceof String ||
				key instanceof Integer ||
				key instanceof Boolean ||
				key instanceof Long ||
				key instanceof Character)
			{
				values.append(tabs + "key   |" + key.toString() + "|\n");
		}
			else
			{
				values.append(tabs + "key   |\n");
			
				convert(key);
				values.append("|\n");
			}
			
			Object value = map.get(key);
			
			if (value instanceof String ||
				value instanceof Integer ||
				value instanceof Boolean ||
				value instanceof Long ||
				value instanceof Character)
			{
				values.append(tabs + "value |" + value.toString() + "|\n");
			}
			else
			{
				values.append(tabs + "value |\n");
				convert(value);
				values.append("|\n");
			}

			values.append(tabs + SEPARATOR + "\n");
		}
		
		decreaseTabLevel();
		decreaseTabLevel();
	}

	/**
	 * Increases the indentation level of the output. 
	 *
	 */
	private void increaseTabLevel()
	{
		tabs += "    ";
	}

	/**
	 * Decreases the indentation level of the output.
	 *
	 */
	private void decreaseTabLevel()
	{
		if (tabs.length() >= 4)
		{
			tabs = tabs.substring(4);
		}
	}

	/**
	 * Output the name of the current object.
	 * 
	 * @param data    The object name
	 */
	private void printTitle(String data)
	{
		values.append(tabs + SEPARATOR + "\n");
		values.append(tabs + "* " + data + "\n");
		values.append(tabs + SEPARATOR + "\n");
	}

	/**
	 * Output a (field, value) pair.
	 * 
	 * @param name    The name of the field
	 * @param value   The value of the field
	 * @param maxLength    The length of the largest field name in the current
	 *                     object
	 */
	private void printData(String name, String value, int maxLength)
	{
		while (name.length() < maxLength)
		{
			name = name + " ";
		}

		values.append(tabs + name + "  |" + value + "|\n");
	}

		
	/**
	 * 
	 * @author Rick Arnold
	 *
	 * Private subclass for holding (field, value) pairs.
	 */
	private class DataItem
	{
		public String name  = "";
		public String value = "";

		public DataItem(String n, String v)
		{
			name = n;
			value = v;
		}

		public DataItem(String n, Object o)
		{
			name = n;
			value = o.toString();
		}
	};

	/**
	 * 
	 * @author Rick Arnold
	 *
	 * Private subclass for comparing (field, value) pairs to each other. 
	 */
	private class DataItemComparator implements Comparator<DataItem>
	{
		/**
		 * Compares the names of two DataItems
		 * 
		 * @param dataItem1	 The first DataItem
		 * @param dataItem2  The second DataItem
		 * @return  The result of performing a string comparison on the 
		 * 			DataItems name field
		 */
		public int compare(DataItem dataItem1, DataItem dataItem2)
		{
			return (dataItem1).name.compareTo((dataItem2).name);
		}
	};
}
